﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EmployeeService.Test
{
    /// <summary>Unit Tests Class</summary>
    public class TestFactory
    {

        /// <summary>Creates the HTTP request.</summary>
        /// <param name="accessToken">The access token.</param>
        /// <param name="correlationId">The correlation identifier.</param>
        /// <returns></returns>
        public static DefaultHttpRequest CreateHttpRequest()
        {
            var request = new DefaultHttpRequest(new DefaultHttpContext());
            return request;
        }

        /// <summary>Creates the logger.</summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static ILogger CreateLogger(LoggerTypes type = LoggerTypes.Null)
        {
            var logger = type == LoggerTypes.List ? new ListLogger() : NullLoggerFactory.Instance.CreateLogger("Null Logger");

            return logger;
        }

        public static string GetAppSettings()
        {
            var settingsFilePath = $"{AppDomain.CurrentDomain.BaseDirectory}\\local.settings.json";

            using StreamReader r = new StreamReader(settingsFilePath);

            return r.ReadToEnd();
        }
    }
}

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Xunit;

namespace EmployeeService.Test
{
    class Settings
    {
        public string AppId;
        public string TenantId;
        public string ClientSecret;
        public string CosmosEndpoint;
        public string CosmosPrimaryKey;
        public string CosmosDatabaseId;
        public string CosmosContainerId;
        public string DefaultLimit;
        public string RetryCount;
        public string RetryDelay;
    }

    public class UnitTests
    {
        private readonly ILogger _logger = TestFactory.CreateLogger();

        private readonly Settings _settings;

        public UnitTests()
        {
            var value = TestFactory.GetAppSettings();
            _settings = JsonConvert.DeserializeObject<Settings>(value);
            Environment.SetEnvironmentVariable("AppId", _settings.AppId);
            Environment.SetEnvironmentVariable("ClientSecret", _settings.ClientSecret);
            Environment.SetEnvironmentVariable("TenantId", _settings.TenantId);
            Environment.SetEnvironmentVariable("CosmosEndpoint", _settings.CosmosEndpoint);
            Environment.SetEnvironmentVariable("CosmosPrimaryKey", _settings.CosmosPrimaryKey);
            Environment.SetEnvironmentVariable("CosmosDatabaseId", _settings.CosmosDatabaseId);
            Environment.SetEnvironmentVariable("CosmosContainerId", _settings.CosmosContainerId);
            Environment.SetEnvironmentVariable("DefaultLimit", _settings.DefaultLimit);
        }

        [TestCase]
        public void Test_GetAllUsers()
        {
            var request = TestFactory.CreateHttpRequest();
            var response = (JsonResult)EmployeesFunction.Run(request, _logger);
            Assert.IsTrue(response.Value != null);
        }

        [TestCase]
        public void Test_GetUserById()
        {
            var request = TestFactory.CreateHttpRequest();
            var response = (JsonResult) Functions.EmployeeFunction.Run(request, Guid.NewGuid().ToString(), _logger);
            Assert.IsTrue(response.Value != null);
        }
    }
}
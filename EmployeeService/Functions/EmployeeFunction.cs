using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Linq;
using System;
using Newtonsoft.Json;
using Polly;
using Polly.Timeout;
using Polly.Wrap;
using EmployeeService.Utilities;

namespace EmployeeService.Functions
{
    public static class EmployeeFunction
    {
        [FunctionName("Employee")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "employees/{id}")] HttpRequest req, string id, ILogger log)
        {
            log.LogInformation("Employee function started processing a request.");
          
            try
            {
                Models.AzureADUser user = null;
                string queryId = String.Format($"employees/{id}");
                string timestamp = "";

                Policy timeoutPolicy = Policy
                    .Timeout(ServiceConfigs.TimeoutValue, TimeoutStrategy.Pessimistic);

                Policy waitAndRetryPolicy = Policy
                    .Handle<Exception>()
                    .WaitAndRetry(ServiceConfigs.RetryCount, retryAttempt => TimeSpan.FromSeconds(ServiceConfigs.RetryDelay), (exception, retryAttempt) =>
                    {
                        log.LogInformation("Could not connect to Microsoft Graph API. Retrying...");
                    });

                Policy fallbackPolicy = Policy
                    .Handle<Exception>()
                    .Fallback(() =>
                    {
                        log.LogInformation("Could not connect to Microsoft Graph API. Falling back to stored data.");

                        var cosmosApi = new CosmosAPI();
                        var storedData = cosmosApi.RetrieveStoredData(queryId, queryId).GetAwaiter().GetResult();

                        if (storedData != null)
                        {
                            user = JsonConvert.DeserializeObject<Models.AzureADUser>(storedData.StoredData);
                            timestamp = storedData.RefreshTimestamp;
                        }
                    });

                PolicyWrap wrapper = Policy.Wrap(fallbackPolicy, waitAndRetryPolicy, timeoutPolicy);

                wrapper.Execute(() =>
                {
                    var authProvider = new UsersGraphAPI();
                    user = authProvider.GetUserById(id);

                    timestamp = DateTimeOffset.Now.ToString("o");

                    CosmosAPI.StoreDataAndForget(
                        new Models.CosmosUserData
                        {
                            Id = queryId,
                            QueryId = queryId,
                            RefreshTimestamp = timestamp,
                            StoredData = JsonConvert.SerializeObject(user)
                        },
                        log);
                });

                if (user != null)
                {
                    return new JsonResult(new {
                        user.Id,
                        firstName = user.GivenName,
                        lastName = user.Surname,
                        user.DisplayName,
                        user.UserPrincipalName,
                        user.Mail,
                        user.JobTitle,
                        user.MobilePhone,
                        user.Department,
                        officePhone = user.BusinessPhones.FirstOrDefault(),
                        refreshTimestamp = timestamp
                    });
                }
                return new JsonResult("");
            }
            catch (System.Exception excption)
            {
                log.LogError(excption.ToString());
                return new BadRequestObjectResult("internal server error") { StatusCode = 500 };
            }
        }
    }
}

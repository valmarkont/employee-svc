using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Linq;
using Newtonsoft.Json;
using Polly;
using Polly.Timeout;
using Polly.Wrap;
using EmployeeService.Utilities;

namespace EmployeeService
{
    public static class EmployeesFunction
    {
        [FunctionName("Employees")]
        public static IActionResult Run([HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequest req, ILogger log)
        {
            log.LogInformation("Employees function started processing a request.");

            try
            {
                int limit = Utilities.ServiceConfigs.DefaultLimit;
                int timeoutValue = Utilities.ServiceConfigs.TimeoutValue;
                int retryCount = Utilities.ServiceConfigs.RetryCount;
                int retryDelay = Utilities.ServiceConfigs.RetryDelay;

                if (!string.IsNullOrEmpty(req.Query["limit"]))
                    int.TryParse(req.Query["limit"].ToString(), out limit);

                string lastName = req.Query["lastName"];
                string firstName = req.Query["firstName"];

                var users = new List<Models.AzureADUser>();
                string id = String.Format($"Employees?firstName={firstName}&lastName={lastName}&limit={limit}");
                string timestamp = "";

                Policy timeoutPolicy = Policy
                    .Timeout(timeoutValue, TimeoutStrategy.Pessimistic);
                
                Policy waitAndRetryPolicy = Policy
                    .Handle<Exception>()
                    .WaitAndRetry(retryCount, retryAttempt => TimeSpan.FromSeconds(retryDelay), (exception, retryAttempt) =>
                    {
                        log.LogInformation("Could not connect to Microsoft Graph API. Retrying...");
                    });
                
                Policy fallbackPolicy = Policy
                    .Handle<Exception>()
                    .Fallback(() =>
                    {
                        log.LogInformation("Could not connect to Microsoft Graph API. Falling back to stored data.");

                        var cosmosApi = new Utilities.CosmosAPI();
                        var storedData = cosmosApi.RetrieveStoredData(id, id).GetAwaiter().GetResult();

                        if (storedData != null)
                        {
                            users = JsonConvert.DeserializeObject<List<Models.AzureADUser>>(storedData.StoredData);
                            timestamp = storedData.RefreshTimestamp;
                        }
                    });

                PolicyWrap wrapper = Policy.Wrap(fallbackPolicy, waitAndRetryPolicy, timeoutPolicy);

                wrapper.Execute(() =>
                {
                    var authProvider = new Utilities.UsersGraphAPI();
                    users = authProvider.GetUsers(firstName, lastName, limit);

                    timestamp = DateTimeOffset.Now.ToString("o");

                    CosmosAPI.StoreDataAndForget(
                        new Models.CosmosUserData
                        {
                            Id = id,
                            QueryId = id,
                            RefreshTimestamp = timestamp,
                            StoredData = JsonConvert.SerializeObject(users)
                        },
                        log);
                });
                if (users.Any())
                {
                    return new JsonResult(users.Take(limit).Select(r => new
                    {
                        r.Id,
                        firstName = r.GivenName,
                        lastName = r.Surname,
                        r.DisplayName,
                        r.UserPrincipalName,
                        r.Mail,
                        r.JobTitle,
                        r.MobilePhone,
                        r.Department,
                        officePhone = r.BusinessPhones.FirstOrDefault(),
                        refreshTimestamp = timestamp
                    }));
                }

                return new JsonResult("");
            }
            catch (System.Exception excption)
            {
                log.LogError(excption.ToString());
                return new BadRequestObjectResult("internal server error") { StatusCode = 500 };
            }
        }
    }
}

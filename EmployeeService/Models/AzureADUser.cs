﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeService.Models
{
    public class AzureADUser
    {
        public string[] BusinessPhones;
        public string DisplayName;
        public string GivenName;
        public string JobTitle;
        public string Mail;
        public string MobilePhone;
        public string OfficeLocation;
        public string PreferredLanguage;
        public string Surname;
        public string UserPrincipalName;
        public string Id;
        public string Department;
    }
}

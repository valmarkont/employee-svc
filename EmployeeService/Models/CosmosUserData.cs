﻿using Newtonsoft.Json;

namespace EmployeeService.Models
{
    public class CosmosUserData
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        public string QueryId { get; set; }
        public string RefreshTimestamp { get; set; }
        public string StoredData { get; set; }
    }
}

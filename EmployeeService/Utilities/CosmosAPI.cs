﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Logging;
using EmployeeService.Models;

namespace EmployeeService.Utilities
{
    public class CosmosAPI
    {
        private static readonly string EndpointUri = ServiceConfigs.CosmosEndpoint;
        private static readonly string PrimaryKey = ServiceConfigs.CosmosPrimaryKey;
        private static readonly string DatabaseId = ServiceConfigs.CosmosDatabaseId;
        private static readonly string ContainerId = ServiceConfigs.CosmosContainerId;

        private readonly CosmosClient client;
        private Database database;
        private Container container;

        public CosmosAPI()
        {
            client = new CosmosClient(EndpointUri, PrimaryKey);
        }

        private async Task<Database> FindOrCreateDatabase()
        {
            return await client.CreateDatabaseIfNotExistsAsync(DatabaseId);
        }

        private async Task<Container> FindOrCreateContainer()
        {
            return await database.CreateContainerIfNotExistsAsync(ContainerId, "/QueryId");
        }

        private async Task InitDatabaseAndContainer()
        {
            database = await FindOrCreateDatabase();
            container = await FindOrCreateContainer();
        }

        public async Task<CosmosUserData> RetrieveStoredData(string id, string queryId)
        {
            try
            {
                await InitDatabaseAndContainer();
                ItemResponse<CosmosUserData> response = await container.ReadItemAsync<CosmosUserData>(id, new PartitionKey(queryId));
                return response.Resource;
            }
            catch (CosmosException ex)
            {
                if (ex.StatusCode == HttpStatusCode.NotFound)
                {
                    return null;
                }
            }

            return null;
        }

        public async Task StoreData(CosmosUserData data)
        {
            await InitDatabaseAndContainer();
            await container.UpsertItemAsync<CosmosUserData>(data, new PartitionKey(data.QueryId));
        }

        public static void StoreDataAndForget(CosmosUserData data, ILogger logger)
        {
            try
            {
                var cosmosApi = new CosmosAPI();
                cosmosApi.StoreData(data).ContinueWith(t => logger.LogError($"Error storing data in Cosmos DB. Exception: {t.Exception.Message}"),
                    TaskContinuationOptions.OnlyOnFaulted);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error storing data in Cosmos DB. Exception: {ex.Message}");
            }
        }
    }
}

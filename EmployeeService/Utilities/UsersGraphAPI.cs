﻿using EmployeeService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace EmployeeService.Utilities
{
    public class UsersGraphAPI
    {
        private string _Accesstoken;

        HttpClient _httpClient;

        List<AzureADUser> _usersResult;

        public UsersGraphAPI()
        {
            _Accesstoken = GetAccessToken().Result;
            if (string.IsNullOrEmpty(_Accesstoken))
            {
                throw new System.Exception("Failed to generate an access token.");
            }

            _httpClient = GetAuthorizedhttpClient(_Accesstoken);
            _usersResult = new List<AzureADUser>();
        }

        private async Task<string> GetAccessToken()
        {
            _httpClient = new HttpClient();
            Dictionary<string, string> payload = new Dictionary<string, string>()
                    {
                        { "grant_type","client_credentials" },
                        { "client_id", ServiceConfigs.AppId},
                        { "client_secret", ServiceConfigs.ClientSecret },
                        { "scope", "https://graph.microsoft.com/.default" }
                    };

            var tenantId = ServiceConfigs.TenantId;
            var url = "https://login.microsoftonline.com/" + tenantId + "/oauth2/v2.0/token";

            var req = new HttpRequestMessage(HttpMethod.Post, url)
            {
                Content = new FormUrlEncodedContent(payload)
            };

            var response = await _httpClient.SendAsync(req);
            JObject result = JObject.Parse(response.Content.ReadAsStringAsync().Result);
            return result["access_token"].ToString();
        }

        public AzureADUser GetUserById(string userId)
        {
            var url = $"https://graph.microsoft.com/v1.0/users/{userId}?$select=Id,GivenName,Surname,DisplayName,UserPrincipalName,Mail,JobTitle,MobilePhone,department,BusinessPhones";
            HttpResponseMessage response = _httpClient.GetAsync(url).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            dynamic data = JObject.Parse(content);
            if (data.error != null && data.error.code == "Request_ResourceNotFound")
            {
                return null;
            }

            return JsonConvert.DeserializeObject<AzureADUser>(data.ToString());
        }

        private static HttpClient GetAuthorizedhttpClient(string token)
        {
            var accessToken = token;
            var authValue = new AuthenticationHeaderValue("Bearer", accessToken);
            var httpclient = new HttpClient()
            {
                DefaultRequestHeaders = { Authorization = authValue }
            };
            return httpclient;
        }

        public List<AzureADUser> GetUsers(string firstName, string lastName, int limit)
        {
            var url = "https://graph.microsoft.com/v1.0/users";

            string filter = string.Empty;


            string filterName = string.Empty;

            if (!string.IsNullOrEmpty(firstName))
            {
                filterName = $"$filter=givenName eq '{firstName}'";
                if (!string.IsNullOrEmpty(lastName))
                {
                    filterName += $" and surname eq '{lastName}'";
                }
            }

            if (!string.IsNullOrEmpty(lastName) && string.IsNullOrEmpty(filterName))
            {
                filterName = $"$filter=surname eq '{lastName}'";
            }

            var queryUrl = $"{url}?$select=Id,GivenName,Surname,DisplayName,UserPrincipalName,Mail,JobTitle,MobilePhone,department,BusinessPhones";
            if (!string.IsNullOrEmpty(filter))
            {
                queryUrl += $"&{filter}";
            }

            if (!string.IsNullOrEmpty(filterName))
            {
                queryUrl += $"&{filterName}";
            }

            HttpResponseMessage response = _httpClient.GetAsync(queryUrl).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            dynamic data = JObject.Parse(content);
            dynamic nextPage = (string)data["@odata.nextLink"];

            var users = JsonConvert.DeserializeObject<List<AzureADUser>>(data.value.ToString());
            _usersResult.AddRange(users);
            if (!string.IsNullOrEmpty(nextPage) && _usersResult.Count < limit)
            {
                do
                {
                    response = _httpClient.GetAsync(nextPage).Result;
                    content = response.Content.ReadAsStringAsync().Result;
                    data = JObject.Parse(content);
                    nextPage = (string)data["@odata.nextLink"];
                    users = JsonConvert.DeserializeObject<List<AzureADUser>>(data.value.ToString());
                    _usersResult.AddRange(users);
                } while (!string.IsNullOrEmpty(nextPage) && _usersResult.Count < limit);
            }

            return _usersResult;
        }
    }
}
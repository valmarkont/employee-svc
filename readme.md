# Employee Service

## Overview

* End-point hosted as Azure function on Linux [consumption plan](https://docs.microsoft.com/en-us/azure/azure-functions/functions-scale#consumption-plan)
* Using dotnet core v3.1
* End-point specifications: https://app.swaggerhub.com/apis/vkhazin/employee-svc/1.0.0
* The end-point to leverage [Microsoft Graph](https://developer.microsoft.com/en-us/graph/graph-explorer)
* Retrieve users from Azure Active Directory using following API: https://graph.microsoft.com/v1.0/users
* End-point requires no authentication
* End-point to authenticate to Graph Api using application identity
* Namespace: EmployeeService

## Azure AD Setup

* Proceed to [Azure AD -> App Registrations](https://portal.azure.com/#blade/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/RegisteredApps)
* Register a new application: `New registration`
* Provide a name
* Copy `Application Client ID` and `Directory (tenant) ID` you will need it to configure the function variables
* Select `View API Permissions`
* Select `Add a permission`
* Select `Microsoft Graph` -> `Application permissions` -> `Search for User.Read.All` -> `Add permissions`
* Select `Grant Admin Consent`
* There could be a delay until consent is granted, wait for the green icons before proceeding
* Select `Certificates & secrets` -> `New client secret` -> `New client secret`
* Make sure to copy the created secret as it won't be shown forever in clear text

## Azure Cosmos DB Setup
* Proceed to [Home -> Azure Cosmos DB](https://portal.azure.com/#blade/HubsExtension/BrowseResource/resourceType/Microsoft.DocumentDb%2FdatabaseAccounts)
* Create a new Cosmos DB account if it doesn't exist `Create Azure Cosmos DB Account`
* Provide a resource group and Cosmos DB account name
* Select `Core (SQL)` as API
* Select `Create`
* After the account has been created, select `Go to resource`
* Select `Settings` -> `Keys`
* Copy values for `URI` and `PRIMARY KEY` which are needed to configure the function variables
* Select values for database ID and container ID. You don't have to configure them in the portal, the function will create them automatically if they do not exist

## Create Function

* Create a new function using Azure Portal
* Use Linux Consumption plan e.g. in US East 2 region

## Function Configuration

* In order to authenticate against active directory, client id, tenant id and secret will be required
* The values are stored in env variables
* To configure CORS and environment variables for an existing function:
```
az login
export resource_group_name=rg-poc-employee-service
export function_name=kpgos-poc-employee-svc

az functionapp cors remove -g ${resource_group_name} -n ${function_name} --allowed-origins &&
az functionapp cors add --name ${function_name} \
                        --resource-group ${resource_group_name} \
                        --allowed-origins "*" &&

az functionapp config appsettings set -g ${resource_group_name} \
  -n ${function_name} \
  --settings "AppId=--app client id--" \
  "ClientSecret=--client-secret--" \
  "TenantId=--Directory (tenant) ID--" \
  "DefaultLimit=100"

az functionapp config appsettings set -g ${resource_group_name} \
  -n ${function_name} \
  --settings "CosmosEndpoint=--URI from Cosmos DB configuration--" \
  "CosmosPrimaryKey=--PRIMARY KEY from Cosmos DB configuration--" \
  "CosmosDatabaseId=--database ID for storing the data--" \
  "CosmosContainerId=--container ID for storing the data--"

az functionapp config appsettings set -g ${resource_group_name} \
  -n ${function_name} \
  --settings "TimeoutValue=--Polly timeout value in seconds--" \
  "RetryCount=--Polly number of retries before fallback--" \
  "RetryDelay=--Polly number of seconds to wait between retries--"
```

## Function deployment

* Deploy the function using [az-cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest):
```
az login

export resource_group_name=rg-poc-employee-service
export function_name=kpgos-poc-employee-svc

cd ./EmployeeService

func azure functionapp publish ${function_name} \
  --dotnet-cli-params -- "--configuration Release"
```
* Two urls will be printed as an output of the deployment process